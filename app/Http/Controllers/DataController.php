<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Data;
use App\Models\Section;
use App\Models\GlobalParameter;
use DB;

class DataController extends Controller
{
    public function index(Request $request){
		$section_1_header = Section::where('is_active','Y')->where('section_class','data')->where('section_view_name','section_1_view')->first();

		$rows = Data::where('is_active','Y')->orderBy('publish_date','DESC')->whereRaw("YEAR(publish_date) = ".date('Y')." ")->paginate(5);
		$category = GlobalParameter::where('is_active','Y')->where('flag','kategori_data')->orderBy('value','asc')->get();
		$year = Data::select(DB::raw('YEAR(publish_date) as year'))->groupBy(DB::raw('YEAR(publish_date)'))->get();

		return view('data',compact('section_1_header','rows','category','year'));
	}

	public function search(Request $request)
    {
		$section_1_header = Section::where('is_active','Y')->where('section_class','data')->where('section_view_name','section_1_view')->first();

        $search_category = $request->cat;
		$search_year = isset($request->year) ? $request->year : date('Y');

		$rows = Data::where('is_active','Y')
					->where('report_type',$search_category)
					->whereRaw("YEAR(publish_date) = $search_year")
					->orderBy('publish_date','DESC')
					->paginate(5);
		$category = GlobalParameter::where('is_active','Y')->where('flag','kategori_data')->orderBy('value','asc')->get();
		$year = Data::select(DB::raw('YEAR(publish_date) as year'))->groupBy(DB::raw('YEAR(publish_date)'))->get();

		return view('data',compact('section_1_header','rows','category','search_category','search_year','year'));
    }
}
