<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Section;
use DB;

class IPKNController extends Controller
{
    public function index(Request $request){
		$section_1_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_1_view')->first();
		$section_1_body = NULL;
		if(isset($section_1_header)){
			$section_1_body = Content::where('is_active','Y')->where('section_id',$section_1_header->section_id)->orderBy('content_id','asc')->first();
		}

		$section_2_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_2_view')->first();
		$section_2_body = NULL;
		if(isset($section_2_header)){
			$section_2_body = Content::where('is_active','Y')->where('section_id',$section_2_header->section_id)->orderBy('content_id','asc')->limit(2)->get();
		}

		$section_3_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_3_view')->first();
		$section_3_body = NULL;

		if(isset($section_3_header)){
			$section_3_body = Content::where('is_active','Y')->where('section_id',$section_3_header->section_id)->orderBy('content_id','asc')->get();
		}

		$section_4_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_4_view')->first();
		$section_5_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_5_view')->first();
		$section_6_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_6_view')->first();

		$last_year = DB::table('cms_capaian_ipkn')->select('year')->orderBy('year','DESC')->groupBy('year')->first();

		$capaian_ipkn = DB::table('cms_capaian_ipkn')->join('mst_provinces','mst_provinces.id','=','cms_capaian_ipkn.province_id')->where('cms_capaian_ipkn.year',$last_year->year)->where('cms_capaian_ipkn.is_active','Y')->where('mst_provinces.is_active','Y')->orderBy('rank','asc')->get();
		$province = DB::table('ipkn_tr_data')->select('p.id as province_id','p.name as province_name')->join('mst_provinces as p','p.id','=','ipkn_tr_data.province_id')->where('p.is_active','Y')->orderBy('p.name','ASC')->groupBy('ipkn_tr_data.province_id')->get();

		$index = DB::table('ipkn_mst_subindex')->where('is_active','Y')->get();
		
		$year = DB::table('ipkn_tr_data_header')->select('dh_year')->orderBy('dh_year','DESC')->groupBy('dh_year')->get();
		$last_year_ipkn_header = DB::table('ipkn_tr_data_header')->select('dh_year')->orderBy('dh_year','DESC')->groupBy('dh_year')->first();

		return view('ipkn',compact('section_1_header','section_1_body','section_2_header','section_2_body','section_3_header','section_3_body','section_4_header','section_5_header','section_6_header','capaian_ipkn','province','index','year','last_year_ipkn_header'));
	}

	public function information(Request $request){
		$id = $request->id;

		$row = DB::table('ipkn_mst_indicator')->where('indicator_id',$id)->first();

		$html = '
				<table class="table table-borderless">
					<tbody>
						<tr>
							<td colspan="3"><span style="font-weight: bold">'.$row->indicator_code.' '.$row->indicator_name.'</span></td>
						</tr>

						<tr>
							<td>Deskripsi Indikator</td>
							<td>:</td>
							<td>'.$row->indicator_desc.'</td>
						</tr>
					
						<tr>
							<td>Principal</td>
							<td>:</td>
							<td>'.$row->indicator_principal.'</td>
						</tr>

						<tr>
							<td width="200px">Unit</td>
							<td width="10px">:</td>
							<td>'.$row->indicator_unit.'</td>
						</tr>
					
						<tr>
							<td>Rasio</td>
							<td>:</td>
							<td>'.$row->indicator_ratio.'</td>
						</tr>
					
						<tr>
							<td>Tipe Nilai</td>
							<td>:</td>
							<td>'.$row->indicator_type_value.'</td>
						</tr>
					
						<tr>
							<td>Pemilik</td>
							<td>: </td>
							<td>'.$row->indicator_owner.'</td>
						</tr>

						<tr>
							<td>Sumber</td>
							<td>:</td>
							<td><a target="_blank" href="'.$row->indicator_source.'">Buka</a></td>
						</tr>
					
						<tr>
							<td>Statistik Saat Ini</td>
							<td>:</td>
							<td>Min '.$row->indicator_min_value.'; Max '.$row->indicator_max_value.'; Median '.$row->indicator_med_value.'</td>
						</tr>
				
					</tbody>
				</table>
				';

		return $html;
	}

	public function capaian(Request $request){
		$province_id = isset($request->id) ? $request->id : '';
		$last_year = DB::table('cms_capaian_ipkn')->select('year')->orderBy('year','DESC')->groupBy('year')->first();
		$rows = DB::table('ipkn_mst_pillar')->where('is_active','Y')->get();

		$arr = array();
		if($rows){
			foreach($rows as $r){
				$arr[] = array("pillar" => $r->pillar_desc, "value" => get_ipkn_score_pillar_by_province($r->pillar_id,$province_id,'score', $last_year->year));
			}
		}

		return json_encode($arr,JSON_NUMERIC_CHECK);
	}

	public function index_table(Request $request){
		
		// print_r($request);die;
		$last_year = DB::table('cms_capaian_ipkn')->select('year')->orderBy('year','DESC')->groupBy('year')->first();
		$year = isset($request->year) ? $request->year : $last_year->year;

		$index = DB::table('ipkn_mst_subindex')->where('is_active','Y')->get();

		$html = "";
		$html .= '<table class="table custom table-striped fold-table">
					<thead>
						<tr style="background: linear-gradient(90deg, #2F5B9E 0%, #132B50 100%);color:#fff">
							<th style="width: 60px; text-align: center;">No</th>
							<th>Index Component</th>
							<th style="width: 165px; text-align: right">Score '.$year.'</th>
							<th style="width: 30px;">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr class="view">
							<td style="text-align: center"></td>
							<td style="text-align: left; font-weight: bold; font-size: 16px">Indeks Pembangunan Kepariwisataan Nasional</td>
							<td style="width:165px; text-align: right">'.get_ipkn_global_index('score',$year).'</td>
							<td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
						</tr>';
						if($index){
							$no=1;foreach($index as $i){
		$html .=	'		<tr class="view" style="background: #122b5254">
								<td style="text-align: center">'.$no++.'</td>
								<td style="text-align: left">'.$i->index_desc.'</td>
								<td style="width:165px; text-align: right">'.get_ipkn_score_index($i->index_id,'score',$year).' <span class="arrow-down-color" style="color: red"></span></td>
								<td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
							</tr>';
							$pillar = get_pillar_ipkn($i->index_id);
							if($pillar){
							foreach($pillar as $p){
		$html .= '				<tr class="view">
									<td style="text-align: center"></td>
									<td style="text-align: left">
										'.$p->pillar_desc.'
									</td>
									<td style="width:165px; text-align: right">'.get_ipkn_score_pillar($p->pillar_id,'score',$year).'</td>
									<td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
								</tr>
					
								<tr class="fold">
									<td class="fold-area" colspan="5">
										<div class="fold-content">
											<table class="table custom">
												<tbody>';
													$sub_pillar = get_sub_pillar_ipkn($p->pillar_id);
													if($sub_pillar){
													foreach($sub_pillar as $sp){
		$html .= '									<tr>
														<td style="width: 60px"></td>
														<td style="text-align: left;">
															<a class="info" style="cursor:pointer !important" onclick="show_modal('.$sp->indicator_id.')">
																<i class="fa fa-circle-info color-grey fs-20 color-grey fs-20"></i>
															</a>
															'.$sp->indicator_code.' '.$sp->indicator_name.'
														</td>
														<td style="width:165px; text-align: right">'.get_ipkn_score_sub_pillar($p->pillar_id,$sp->indicator_id,'score',$year).' </td>
														<th style="width:30px; text-align: right">&nbsp;</th>
													</tr>';
													}
													}
		$html .= '										</tbody>
											</table>
										</div>
									</td>
								</tr>';
							}
							}
						}
						}
						
	$html .= '		</tbody>
				</table>
				<div class="modal fade" id="modal-content-view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" style="color:#000" id="text-title-modal">Information</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body">
								<div id="modal-content-body-load-page"></div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
				$(".fold-table tr.view").on("click", function(){
					$(this).toggleClass("open").next(".fold").toggleClass("open");
				});
			  
				function show_modal(id){
				  $.ajax({
					headers: {
						"X-CSRF-TOKEN": $(\'meta[name="csrf-token"]\').attr("content")
					},
					type: "POST",
					url: "'.route("ipkn/information").'",
					data: {id: id}
				  }).done(function(response) {
					$("#modal-content-body-load-page").html(response);
				  });
				  $("#modal-content-view").modal("show");
				}
				</script>';

		return $html;
	}
}
