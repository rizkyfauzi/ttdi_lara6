<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Section;
use DB;

class TTDIController extends Controller
{
    public function index(Request $request){

		$default_year = 2021; 
		$section_1_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_1_view')->first();
		$section_1_body = NULL;
		if(isset($section_1_header)){
			$section_1_body = Content::where('is_active','Y')->where('section_id',$section_1_header->section_id)->orderBy('content_id','asc')->first();
		}

		$section_2_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_2_view')->first();
		$section_2_body = NULL;
		if(isset($section_2_header)){
			$section_2_body = Content::where('is_active','Y')->where('section_id',$section_2_header->section_id)->orderBy('content_id','asc')->first();
		}

		/*$section_3_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_2_view')->first();
		$section_3_body = NULL;
		if(isset($section_3_header)){
			$section_3_body = Content::where('is_active','Y')->where('section_id',$section_3_header->section_id)->orderBy('content_id','asc')->first();
		}*/

		$section_4_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_4_view')->first();
		$section_4_body = NULL;
		if(isset($section_4_header)){
			$section_4_body = Content::where('is_active','Y')->where('section_id',$section_4_header->section_id)->orderBy('content_id','asc')->get();
		}

		$section_5_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_5_view')->first();
		$section_6_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_6_view')->first();
		$section_7_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_7_view')->first();

		$last_year = DB::table('cms_capaian_ttdi')->select('year')->orderBy('year','DESC')->groupBy('year')->first();
		$sub_index = DB::table('mst_index')->where('is_active','Y')->orderBy('index_id','ASC')->get();
		$capaian_ttdi = DB::table('cms_capaian_ttdi')->where('cms_capaian_ttdi.year',$last_year->year)->where('is_active','Y')->get();

		$last_year_index = $default_year; //DB::table('tr_data')->select('year')->orderBy('year','DESC')->groupBy('year')->first();
		$index = DB::table('mst_index')->where('is_active','Y')->get();

		$year = DB::table('tr_data')->select('year')->orderBy('year','DESC')->groupBy('year')->get();
		$table_year = DB::table('cms_capaian_ttdi')->select('year')->orderBy('year','DESC')->groupBy('year')->get();

		// echo '<pre>';print_r($_GET);exit;

		return view('ttdi',compact('section_1_header','section_1_body','section_2_header','section_2_body','section_4_header','section_4_body','section_5_header','section_6_header','section_7_header','sub_index','capaian_ttdi','index','year','table_year','last_year','last_year_index'));
	}

	public function information(Request $request){
		$id = $request->id;

		$row = DB::table('mst_subpillar')->where('subpillar_id',$id)->first();

		$html = '
				<table class="table table-borderless">
					<tbody>
						<tr>
							<td colspan="3"><span style="font-weight: bold">'.$row->subpillar_desc.'</span></td>
						</tr>
						<tr>
							<td width="200px">Deskripsi</td>
							<td width="10px">:</td>
							<td>'.$row->question.'</td>
						</tr>
					
						<tr>
							<td>Sumber</td>
							<td>:</td>
							<td>'.$row->source.'</td>
						</tr>
					
						<tr>
							<td>Edisi</td>
							<td>:</td>
							<td>'.$row->year_label.'</td>
						</tr>
					
						<tr>
							<td>Catatan</td>
							<td>: </td>
							<td>'.nl2br($row->note).'</td>
						</tr>
						<tr>
							<td>Tipe Data</td>
							<td>:</td>
							<td>'.$row->type_data.'</td>
						</tr>
						<tr>
							<td>Nilai Sebelumnya</td>
							<td>:</td>
							<td>'.$row->data_value.'</td>
						</tr>
					
						<tr>
							<td>Nilai Saat Ini</td>
							<td>:</td>
							<td>'.$row->weighted.'</td>
						</tr>
					
						<tr>
							<td>Statistik Saat Ini</td>
							<td>:</td>
							<td>Min '.$row->min_value.'; Max '.$row->max_value.'; Median '.$row->med_value.'</td>
						</tr>
					
						<tr>
							<td>Exclussion Filter</td>
							<td>:</td>
							<td>'.(($row->is_exclusive == "N") ? 'No' : 'Yes').'</td>
						</tr>
					
						<tr>
							<td>Outlayer</td>
							<td>:</td>
							<td>'.(($row->is_outlayer == "N") ? 'No' : 'Yes').'</td>
						</tr>

						<!--<tr>
							<td>Ranking Saat Ini</td>
							<td>:</td>
							<td>'.(($row->is_outlayer == "N") ? 'No' : 'Yes').'</td>
						</tr>

						<tr>
							<td>Ranking Sebelumnya</td>
							<td>:</td>
							<td>'.(($row->is_outlayer == "N") ? 'No' : 'Yes').'</td>
						</tr>-->
				
					</tbody>
				</table>
				';

		return $html;
	}

	public function capaian(Request $request){

		$default_year = 2021;
		$last_year = DB::table('tr_data')->select('year')->orderBy('year','DESC')->groupBy('year')->first();
		$year = isset($request->year) ? $request->year : $default_year; //$last_year->year;

		$rows = DB::table('mst_pillar')->where('is_active','Y')->get();

		$arr = array();
		if($rows){
			foreach($rows as $r){
				$arr[] = array("pillar" => $r->pillar_desc, "value" => get_ttdi_score_pillar_by_province($r->pillar_id,'score',$year));
			}
		}

		return json_encode($arr,JSON_NUMERIC_CHECK);
	}

	public function capaian_table(Request $request){
		$default_year = 2021;
		$last_year = DB::table('cms_capaian_ttdi')->select('year')->orderBy('year','DESC')->groupBy('year')->first();
		$year = isset($request->year) ? $request->year : $default_year; //$last_year->year;

		$capaian_ttdi = DB::table('cms_capaian_ttdi')->where('cms_capaian_ttdi.year',$year)->where('is_active','Y')->get();

		$html = "";
		if($capaian_ttdi){
        $html .=    '<table class="table custom table-bordered table-striped table-responsive" width="100%">
						<thead>
							<tr>
								<td style="background: #EAC170;"><b style="font-size:20px">Sub-Index</b></td>';
                  				foreach($capaian_ttdi as $ct){
        $html .=          		'<td class="text-center" style="width:100px"><b>'.$ct->country_name.'</b></td>';
								}
		$html .=			'</tr>
						</thead>
						<tbody>
							<tr>
								<td><b>Enabling Environment</b></td>';
								foreach($capaian_ttdi as $ct){
		$html .=				'<td class="text-center" '.((get_capaian_ttdi_high_score('index_1',$year)!=NULL and get_capaian_ttdi_high_score('index_1',$year) == $ct->index_1) ? 'style="background: #EAC170;"' : '') .'>'.$ct->index_1.'</td>';
								}
		$html .=			'</tr>
							<tr>
								<td><b>Travel & Tourism Policy and Conditions</b></td>';
								foreach($capaian_ttdi as $ct){
		$html .=				'<td class="text-center" '.((get_capaian_ttdi_high_score('index_2',$year)!=NULL and get_capaian_ttdi_high_score('index_2',$year) == $ct->index_2) ? 'style="background: #EAC170;"' : '') .'>'.$ct->index_2.'</td>';
								}
		$html .=			'</tr>
							<tr>
								<td><b>Infrastructure</b></td>';
                    			foreach($capaian_ttdi as $ct){
		$html .=				'<td class="text-center" '.((get_capaian_ttdi_high_score('index_3',$year)!=NULL and get_capaian_ttdi_high_score('index_3',$year) == $ct->index_3) ? 'style="background: #EAC170;"' : '') .'>'.$ct->index_3.'</td>';
                    			}
		$html .=			'</tr>
                  			<tr>
								<td><b>Travel & Tourism Demand Drivers</b></td>';
                    			foreach($capaian_ttdi as $ct){
		$html .=				'<td class="text-center" '.((get_capaian_ttdi_high_score('index_4',$year)!=NULL and get_capaian_ttdi_high_score('index_4',$year) == $ct->index_4) ? 'style="background: #EAC170;"' : '') .'>'.$ct->index_4.'</td>';
                    			}
		$html .=			'</tr>
                  			<tr>
								<td><b>Travel & Tourism Sustainability</b></td>';
                    			foreach($capaian_ttdi as $ct){
		$html .=				'<td class="text-center" '.((get_capaian_ttdi_high_score('index_5',$year)!=NULL and get_capaian_ttdi_high_score('index_5',$year) == $ct->index_5) ? 'style="background: #EAC170;"' : '') .'>'.$ct->index_5.'</td>';
                    }
		$html .=			'</tr>
              			</tbody>
            		</table>';
        }

		return $html;
	}

	public function index_table(Request $request){

		
		$default_year = 2021;
		
		$year = isset($request->year) ? $request->year : $default_year;
		// echo '<pre>';print_r($year);exit;

		$index = DB::table('mst_index')->where('is_active','Y')->get();

		$html = "";
		$html .= '<table class="table custom table-striped fold-table">
					<thead>
						<tr style="background: linear-gradient(90deg, #2F5B9E 0%, #132B50 100%);color:#fff">
							<th style="width: 60px; text-align: center;">No</th>
							<th>Index Component</th>
							<th style="width: 165px; text-align: right">Score '.($year - 2).' (TTCI)</th>
							<th style="width: 165px; text-align: right">Score '.$year.' (TTDI)</th>
							<th style="width: 30px;">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr class="view">';
							$last_score = get_ttdi_global_index('last_score',$year-2);
							$current_score = get_ttdi_global_index('score',$year);

							$icon = "";
							if($last_score > $current_score){
								$icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
							} else if($last_score < $current_score){
								$icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
							} else {
								$icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
							}
		$html .= '			<td style="text-align: center"></td>
							<td style="text-align: left; font-weight: bold; font-size: 16px">Travel & Tourism Development Index</td>
							<td style="width:165px; text-align: right">'.$last_score.'</td>
							<td style="width:165px; text-align: right">'.$current_score.' &nbsp; '.$icon.'</td>
							<td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
						</tr>';
				
						if($index){
						$no=1;foreach($index as $i){
							$last_score = get_ttdi_score_index($i->index_id,'last_score',$year-2);
							$current_score = get_ttdi_score_index($i->index_id,'score',$year);

							$icon = "";
							if($last_score > $current_score){
								$icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
							} else if($last_score < $current_score){
								$icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
							} else {
								$icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
							}
			$html .= '		<tr class="view" style="background: #122b5254">
							<td style="text-align: center">'.$no++.'</td>
							<td style="text-align: left">'.$i->index_desc.'</td>
							<td style="width:165px; text-align: right">'.$last_score.' </td>
							<td style="width:165px; text-align: right">'.$current_score.'  '.$icon.'</td>
							<td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
						</tr>';
						$pillar = get_pillar_ttdi($i->index_id);
						if($pillar){
						foreach($pillar as $p){
				$html .= '			<tr class="view">
								<td style="text-align: center"></td>
								<td style="text-align: left">
									<img src="https://ttci.kemenparekraf.go.id/uploaded/images/'.$p->pillar_icon.'" width="40px"> '.$p->pillar_desc.' '.$p->pillar_note.'
								</td>
								<td style="width:165px; text-align: right">'.get_ttdi_score_pillar($p->pillar_id,'last_score',$year-2).'</td>
								<td style="width:165px; text-align: right">
									'.get_ttdi_score_pillar($p->pillar_id,'score',$year).' ';
									
										$last_score = get_ttdi_score_pillar($p->pillar_id,'last_score',$year-2);
										$current_score = get_ttdi_score_pillar($p->pillar_id,'score',$year);
				
										$icon = "";
										if($last_score > $current_score){
											$icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
										} else if($last_score < $current_score){
											$icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
										} else {
											$icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
										}
									
			$html .=				' '.$icon.'</td>
								<td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
							</tr>
				
							<tr class="fold">
								<td class="fold-area" colspan="5">
									<div class="fold-content">
										<table class="table custom">
											<tbody>';
												$sub_pillar = get_sub_pillar_ttdi($p->pillar_id);
												if($sub_pillar){
												foreach($sub_pillar as $sp){
				$html .= '								<tr>
													<td style="width: 60px"></td>
													<td style="text-align: left;">
														<a class="info" style="cursor:pointer !important" onclick="show_modal('.$sp->subpillar_id.')">
															<i class="fa fa-circle-info color-grey fs-20 color-grey fs-20"></i>
														</a> '.$sp->subpillar_desc;
														
					$html .= '								</td>';
													
														$last_score = get_ttdi_score_sub_pillar($p->pillar_id,$sp->subpillar_id,'last_score',$year-2);
														$current_score = get_ttdi_score_sub_pillar($p->pillar_id,$sp->subpillar_id,'score',$year);
				
														$icon = "";
														if($last_score > $current_score){
															$icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
														} else if($last_score < $current_score){
															$icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
														} else {
															$icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
														}
													
							$html .= '						<td style="width:165px; text-align: right">'.$last_score.'</td>
													<td style="width:165px; text-align: right">'.$current_score.' '.$icon.' ';
														
							$html .= '						</td>
													<th style="width:30px; text-align: right">&nbsp;</th>
												</tr>';
													}
												}
								$html .= '			</tbody>
										</table>
									</div>
								</td>
							</tr>';
						}
						}
						}
						}
			$html .= '		</tbody>
				</table>
				<div class="modal fade" id="modal-content-view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" style="color:#000" id="text-title-modal">Information</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body">
								<div id="modal-content-body-load-page"></div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">
				$(".fold-table tr.view").on("click", function(){
					$(this).toggleClass("open").next(".fold").toggleClass("open");
				});
			  
				function show_modal(id){
				  $.ajax({
					headers: {
						"X-CSRF-TOKEN": $(\'meta[name="csrf-token"]\').attr("content")
					},
					type: "POST",
					url: "'.route("ttdi/information").'",
					data: {id: id}
				  }).done(function(response) {
					$("#modal-content-body-load-page").html(response);
				  });
				  $("#modal-content-view").modal("show");
				}
				</script>';

		return $html;
	}
}
