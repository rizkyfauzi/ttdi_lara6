<?php

function format_tanggal($date){
    //$get_date = explode('-',$date);

    $hari = array (
		1 =>  'Senin',
		2 => 'Selasa',
		3 => 'Rabu',
		4 => 'Kamis',
		5 => 'Jumat',
		6 => 'Sabtu',
		7 => 'Minggu'
	);

    $bulan = array (
		1 =>  'Januari',
		2 => 'Februari',
		3 => 'Maret',
		4 => 'April',
		5 => 'Mei',
		6 => 'Juni',
		7 => 'Juli',
		8 => 'Agustus',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Desember'
	);

    $hari_ = date('w',strtotime($date));

    $pecahkan = explode('-', $date);

    $text = $hari[$hari_].', '.$pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];

    return $text;
}

function get_attachment_data($id_data){
    $file = DB::table('cms_attachment')->where('relation_tbl','cms_report_data')->where('relation_id',$id_data)->first();
    if($file){
		$text = "";
        if($file->file_type == "image/png" or $file->file_type == "image/jpeg"){
            return $text .= "data:image/jpeg;base64,".base64_encode($file->file_content);
        } elseif($file->file_type == "application/pdf"){
			return $text .= "data:application/pdf;base64,".base64_encode($file->file_content);
		}

        return $text;
    } 

    return "";
}

function get_attachment_data_name($id_data){
    $file = DB::table('cms_attachment')->where('relation_tbl','cms_report_data')->where('relation_id',$id_data)->first();
    
    return $file->file_name;
}

function get_image_content($id_data,$limit){
    $file = DB::table('cms_attachment')
			->where('relation_tbl','cms_content')
			->where('relation_id',$id_data)
			->where('is_active','Y')
			->limit($limit)
			->get();
    
    return $file;
}

function get_capaian_ttdi_high_score($param,$year){
	$row = DB::table('cms_capaian_ttdi')->select("$param")->where('is_active','Y')->where('year',$year)->orderBy("$param","DESC")->first();

	return $row->$param;
}

function get_capaian_ipkn_high_score($param){
	$last_year = DB::table('cms_capaian_ipkn')->select('year')->orderBy('year','DESC')->groupBy('year')->first();

	$row = DB::table('cms_capaian_ipkn')->select("index_1","index_2","index_3","index_4","index_5")->where('is_active','Y')->where('province_id',$param)->where('year',$last_year->year)->first();

	if(isset($row)){
		$value = max((array)$row);
		$key = array_search($value, (array)$row);

		return $key;
	}
	
	return false;
}

function get_pillar_ttdi($index){
	$rows = DB::table('mst_pillar')
				->where('is_active','Y')
				->where('index_id',$index)
				->get();

	return $rows;
}

function get_sub_pillar_ttdi($pillar_id){
	$rows = DB::table('mst_subpillar')
				->where('is_active','Y')
				->where('pillar_id',$pillar_id)
				->get();

	return $rows;
}

function get_pillar_ipkn($index){
	$rows = DB::table('ipkn_mst_pillar')
				->where('is_active','Y')
				->where('index_id',$index)
				->get();

	return $rows;
}

function get_sub_pillar_ipkn($pillar_id){
	$rows = DB::table('ipkn_mst_indicator')
				->where('is_active','Y')
				->where('pillar_id',$pillar_id)
				->get();

	return $rows;
}

function get_ttdi_score_sub_pillar($pillar_id,$subpillar_id,$param,$year){
	$row = DB::table('tr_data')
			->where('pillar_id',$pillar_id)
			->where('subpillar_id',$subpillar_id)
			->where('year',$year)
			->first();

	return isset($row) ? $row->$param : 0;
}

function get_ttdi_score_pillar($pillar_id,$param,$year){
	$row = DB::table('tr_data')
			->where('pillar_id',$pillar_id)
			->where('year',$year)
			->avg($param);

	return isset($row) ? number_format($row,2) : 0;
}

function get_ttdi_score_pillar_by_province($pillar_id,$param,$year){
	$row = DB::table('tr_data')
			->where('pillar_id',$pillar_id)
			->where('year',$year)
			->avg($param);

	return isset($row) ? number_format($row,2) : 0;
}

function get_ttdi_score_index($index_id,$param,$year){
	$pillar = DB::table('mst_pillar')
				->where('is_active','Y')
				->where('index_id',$index_id)
				->get();

	$last_score = 0;
	if($pillar){
		foreach($pillar as $p){
			$last_score += get_ttdi_score_pillar($p->pillar_id,$param,$year);
		}
	}

	if(count($pillar) > 0){
		return number_format($last_score / count($pillar),2);
	} else {
		return false;
	}
}

function get_ttdi_global_index($param,$year){
	$index = DB::table('mst_index')
				->where('is_active','Y')
				->get();

	$last_score = 0;
	if($index){
		foreach($index as $p){
			$last_score += get_ttdi_score_index($p->index_id,$param,$year);
		}
	}

	return number_format($last_score / count($index),2);
}

/*function get_capaian_ttdi(){
	$rows = DB::table('mst_pillar')
				->where('is_active','Y')
				->get();

	$arr = array();
	if($rows){
		foreach($rows as $r){
			$arr[] = array("pillar" => $r->pillar_desc, "value" => get_ttdi_score_pillar($r->pillar_id,'score',2021));
		}
	}

	return json_encode($arr,JSON_NUMERIC_CHECK);
}*/

//IPKN

function get_ipkn_score_sub_pillar($pillar_id,$subpillar_id,$param,$year){
	$row = DB::table('ipkn_tr_data_header as a')
			->join('ipkn_tr_data as b','b.dh_id','=','a.dh_id')
			->where('b.pillar_id',$pillar_id)
			->where('b.indicator_id',$subpillar_id)
			->where('a.dh_year',$year)
			->avg($param);

	return isset($row) ? number_format($row,2) : 0;
}

function get_ipkn_score_pillar($pillar_id,$param,$year){
	$row = DB::table('ipkn_tr_data_header as a')
			->join('ipkn_tr_data as b','b.dh_id','=','a.dh_id')
			->where('b.pillar_id',$pillar_id)
			->where('a.dh_year',$year)
			->avg($param);

	return isset($row) ? number_format($row,2) : 0;
}

function get_ipkn_score_pillar_by_province($pillar_id,$province_id,$param){
	$last_year = DB::table('ipkn_tr_data_header')->select('dh_year')->orderBy('dh_year','DESC')->groupBy('dh_year')->first();

	$query = DB::table('ipkn_tr_data_header as a')
			->join('ipkn_tr_data as b','b.dh_id','=','a.dh_id')
			->where('b.pillar_id',$pillar_id);
			if($province_id != ""){
				$query->where('b.province_id',$province_id);
			}
			$query->where('a.dh_year',$last_year->dh_year);
			$row = $query->avg($param);

	return isset($row) ? number_format($row,2) : 0;
}

function get_ipkn_score_index($index_id,$param,$year){
	$pillar = DB::table('ipkn_mst_pillar')
				->where('is_active','Y')
				->where('index_id',$index_id)
				->get();

	$last_score = 0;
	if($pillar){
		foreach($pillar as $p){
			$last_score += get_ipkn_score_pillar($p->pillar_id,$param,$year);
		}
	}

	return number_format($last_score / count($pillar),2);
}

function get_ipkn_global_index($param,$year){
	$index = DB::table('ipkn_mst_subindex')
				->where('is_active','Y')
				->get();

	$last_score = 0;
	if($index){
		foreach($index as $p){
			$last_score += get_ipkn_score_index($p->index_id,$param,$year);
		}
	}

	return number_format($last_score / count($index),2);
}

/*function get_capaian_ipkn(){
	$rows = DB::table('ipkn_mst_pillar')
				->where('is_active','Y')
				->get();

	$arr = array();
	if($rows){
		foreach($rows as $r){
			$arr[] = array("pillar" => $r->pillar_desc, "value" => get_ipkn_score_pillar($r->pillar_id,'score'));
		}
	}

	return json_encode($arr,JSON_NUMERIC_CHECK);
}*/

use Illuminate\Support\Str;

function short_desc($string){
	return Str::words($string,25,'...');
}