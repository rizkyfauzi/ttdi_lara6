<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'cms_content';
    public $timestamps = false;

    protected $guarded = [];
}
