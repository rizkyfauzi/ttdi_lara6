<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Prakarsa Kemenparekraf Portal Indeks Kepariwisataan Indonesia" />
        <meta name="keywords" content="Prakarsa Kemenparekraf, Kemenparekraf, Indeks Kepariwisataan Indonesia"/>
        <meta name="author" content="Kemenparekraf" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Prakarsa Kemenparekraf</title>
        <!-- Favicon-->
        <link rel="icon" type="image/png" href="<?=asset('assets/img/favicon-32x32.png')?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="<?=asset('js/all.js')?>"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?=asset('css/styles.css')?>" rel="stylesheet" />
		
		@stack('style')
    </head>
    <body id="page-top">
		
		<nav class="navbar navbar-expand-lg bg-logo fixed-top" id="mainNav">
            <div class="container">
				<a class="logo mobile" href="{{ route('beranda') }}">
					<img src="<?=asset('assets/img/kemenparekraf-logo.png')?>" height="56">
				</a>
				<a class="btn navbar-brand fs-14 font-rubik fw-normal-1 mobile" href="<?=env('BACKEND_URL')?>" style="border:1.5px solid #EAC170;color:#EAC170;border-radius:20px;">Login</a>
            </div>
        </nav>
		
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-menu fixed-top top86" id="mainNav">
            <div class="container">
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				  <i class="fas fa-bars color-gold-home"></i>
				</button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('beranda') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'beranda' or Request::segment(1) == "") ? 'active' : ''}}" ><i class="fa fa-house color-grey"></i> Beranda</a>
						</li>
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('ttdi') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'ttdi') ? 'active' : ''}}" ><i class="fa fa-globe color-grey"></i> TTDI</a>
						</li>
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('ipkn') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'ipkn') ? 'active' : ''}}" ><i class="fa fa-map-location-dot color-grey"></i> IPKN</a>
						</li>
						<li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('data') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'data') ? 'active' : ''}}" ><i class="fa fa-signal color-grey"></i> Data</a>
						</li>
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('berita') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'berita') ? 'active' : ''}}" ><i class="fa fa-newspaper color-grey"></i> Berita</a>
						</li>
                    </ul>
                </div>
				<a class="text-center align-items-center login" href="{{ route('beranda') }}"><img src="<?=asset('assets/img/logo_kemenparekraf_warna.png')?>" height="40"></a>
				<a class="navbar-brand fs-14 login font-rubik fw-normal-1" style="color:#EAC170;" href="<?=env('BACKEND_URL')?>">Login</a>
            </div>
        </nav>
		
		@yield('content')
		
        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 text-start mb-5 mb-lg-0">
						<a class="logo-footer mb-4" href="index.html">
							<img src="<?=asset('assets/img/kemenparekraf-logo.png')?>" height="56">
						</a>
                        <p class="menu-footer fw-normal fs-11 text-white">
                            Jl. Medan Merdeka Barat No. 17, RT/RW 02/03, Gambir,<br> Daerah Khusus Ibukota Jakarta 10110, Indonesia.
                        </p>
						<p class="menu-footer fw-normal fs-11 text-white">
                            Whatsapp Contact Center : 0811-895-6767
                        </p>
						<p class="menu-footer fw-normal fs-11 text-white">
                            Email : info@kemenparekraf.go.id
                        </p>
						<ul id="social-media" style="padding-left: 0;">
							<li><a target="_blank" href="https://www.facebook.com/ParekrafRI/"><i class="fa fa-brands fa-facebook-f color-grey fs-20"></i></a></li>
							<li><a target="_blank" href="https://www.youtube.com/channel/UClm8VHUZQnhFmIndX6oLgJA?feature=emb_ch_name_ex"><i class="fa fa-brands fa-youtube color-grey fs-20"></i></a></li>
							<li><a target="_blank" href="https://twitter.com/Kemenparekraf"><i class="fa fa-brands fa-twitter color-grey fs-20"></i></a></li>
							<li><a target="_blank" href="https://www.instagram.com/kemenparekraf.ri/"><i class="fa fa-brands fa-instagram color-grey fs-20"></i></a></li>
							<li><a target="_blank" href="https://www.tiktok.com/@kemenparekraf"><i class="fa fa-brands fa-tiktok color-grey fs-20"></i></a></li>
						</ul>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 text-start mb-5 mb-lg-0">
                        <label class="mb-3 font-rubik color-gold fw-bold fs-13">Menu</label>
						<ul style="padding-left: 0;list-style:none">
							<li class="mb-2"><a href="https://kemenparekraf.go.id/profil/profil-lembaga" class="menu-footer fw-normal-1 fs-13">Tentang Kemenparekraf</a></li>
							<li class="mb-2"><a href="https://ppid.kemenparekraf.go.id/" class="menu-footer fw-normal-1 fs-13">Hubungi Kami</a></li>
							<li><a href="https://kemenparekraf.go.id/faq" class="menu-footer fw-normal-1 fs-13">FAQ</a></li>
						</ul>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4 text-start">
                        <label class="mb-3 font-rubik color-gold fw-bold fs-13">Website Kemenparekraf Lainnya</label>
						<ul style="padding-left: 0;list-style:none">
							<li class="mb-2"><a href="https://www.indonesia.travel/id/id/home" class="menu-footer fw-normal-1 fs-13">Promosi Pariwisata</a></li>
							<li class="mb-2"><a href="https://www.indonesia.travel/event/id/home" class="menu-footer fw-normal-1 fs-13">Promosi Event</a></li>
							<li class="mb-2"><a href="https://www.indonesia.travel/creative/id/home" class="menu-footer fw-normal-1 fs-13">Promosi Ekonomi Kreatif</a></li>
							<li class="mb-2"><a href="https://mice.kemenparekraf.go.id/" class="menu-footer fw-normal-1 fs-13">Promosi MICE/Bisnis</a></li>
							<li><a href="https://tasranselparekraf.id" class="menu-footer fw-normal-1 fs-13">E-Journal</a></li>
						</ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-3 text-center text-white">
            <div class="container">
				<small class="color-gold">© 2022 Kementerian Pariwisata Dan Ekonomi Kreatif/Badan Pariwisata Dan Ekonomi Kreatif</small>
			</div>
        </div>
		<script src="<?=asset('js/popper.min.js')?>" ></script>
        <!-- Bootstrap core JS-->
        <script src="<?=asset('js/bootstrap.bundle.min.js')?>"></script>
        <!-- Core theme JS-->
        <!--<script src="<?=asset('js/scripts.js')?>"></script>-->
		
		@stack('script')
    </body>
</html>
