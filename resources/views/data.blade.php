@extends('app')
@push('style')
<style type="text/css">
.download .btn {
  transform: translate(38%, -120%);
  -ms-transform: translate(38%, -120%);
  color: #fff;
  border: 1px solid #fff;
  cursor: pointer;
  border-radius: 5px;
  text-align: center;
}

.download .btn:hover {
  background-color: #1E293B;
}
</style>
@endpush

@section('content')
<section class="masthead text-white mb-5 mt-min-custom">
  <div class="container">
      <h2 class="text-center mb-2 font-rubik fw-bold color-navy fw-bold">{!! isset($section_1_header) ? nl2br($section_1_header->section_title) : '' !!}</h2>
      <p class="text-center font-inter fs-16 fw-normal color-grey mb-5">{!! isset($section_1_header) ? nl2br($section_1_header->section_subtitle) : '' !!}</p>
      <div class="row justify-content-center mb-5">
					<div class="col-md-6">
            <form method="post">
              <div class="row">
                <div class="col-md-8 mb-3">
                  <select id="category" class="form-select form-select-lg br-20 color-dark-navy fs-14" onchange="search()" aria-label="">
                    <option value="">Semua Kategori</option>
                    <?php if($category){?>
                    <?php foreach($category as $c){?>
                    <option <?=(isset($search_category) and $c->value==$search_category) ? 'selected="selected"' : ''?> value="<?=$c->value?>"><?=$c->label?></option>
                    <?php }?>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-4">
                  <select id="year" class="form-select form-select-lg br-20 color-dark-navy fs-14" onchange="search()" aria-label="">
                    <option value="">Tahun</option>
                    <?php if($year){?>
                    <?php foreach($year as $y){?>
                    <option <?=(isset($search_year) and $y->year==$search_year) ? 'selected="selected"' : ((date('Y')==$y->year) ? 'selected="selected"' : '')?> value="<?=$y->year?>"><?=$y->year?></option>
                    <?php }?>
                    <?php }?>
                  </select>
                </div>
              </div>
            </form>
					</div>
      </div>

      <?php if(count($rows) > 0){?>
      <?php foreach ($rows as $val){?>
      <div class="row">
        <div class="col-md-12">
          <div class="card mb-4" style="display:block">
              <div class="row">
                <div class="col-md-3">
                  <div class="padding-img-news">
                    <div class="br-10 download" style="overflow:hidden;height:240px;width:180px">
                        <?php if($val->report_cover != NULL){?>
                        <img src="data:image/jpeg;base64,{{base64_encode($val->report_cover)}}" class="" style="overflow:hidden;height:auto;width:100%">
                        <?php }?>
                        <?php if($val->report_cover != NULL){?>
                        <!--<img src="data:image/jpeg;base64,{{base64_encode($val->report_cover)}}" width="auto" height="240">-->
                        <?php } else {?>
                        <!--<img src="<?=asset('assets/img/data.png')?>" width="auto" height="240">-->
                        <?php }?>
                        <?php if(get_attachment_data($val->id) != ""){?>
                        <a class="btn btn-sm" href="<?=get_attachment_data($val->id)?>" download="<?=get_attachment_data_name($val->id)?>">
                          <i class="fa fa-download"></i> Download
                        </a>
                        <?php }?>
                    </div>
                  </div>
                </div>
                <div class="col-md-9">
                  <div class="card-body ml-min-30" style="color: black !important">
                    <h5 class="card-title line-clamp-3 color-dark-navy font-inter fs-20 fw-normal-2"><?=$val->title?></h5>
                    <p class="card-text font-inter line-clamp-3 fs-16 color-grey"><?=$val->description?></p>
                    <div class="row align-items-end" style="height:6rem">
                      <div class="col">
                        <table class="table table-borderless" style="margin-bottom:0px;">
                          <tr>
                            <td rowspan="2" class="text-left" style="vertical-align:middle;width:60px">
                              <img src="<?=asset('assets/img/logo_kemenparekraf_warna.png')?>" height="50">
                            </td>
                            <td colspan="2">
                              <small class="font-inter color-dark-navy fw-normal-2 fs-14 mb-1">{{$val->owner}}</small>
                            </td>
                            <td rowspan="2" style="vertical-align:middle;text-align:right">
                              
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <small class="font-inter color-grey fw-normal fs-12">{{ format_tanggal($val->publish_date) }}</small>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <?php }?>
      <?php } else {?>
        <p class="text-center color-dark-navy">Belum ada data</p>
      <?php }?>

      <div class="container">
				<div class="row">
					<div class="col-md-12">
            <div class="row float-end-custom">
              {{ $rows->onEachSide(1)->links() }}
            </div>
          </div>
        </div>
      </div>
  </div>
</section>
@endsection

@push('script')
<script src="<?=asset('js/jquery.min.js')?>"></script>
<script type="text/javascript">
function search(){
  var category = $("#category").val();
  var year = $("#year").val();

  window.location.href="{{route('data/search')}}?cat="+category+"&year="+year;
}
</script>
@endpush
