@extends('app')
@push('style')
	<link href="<?=asset('css/aos.css')?>" rel="stylesheet">
  <style type="text/css">
    table.loadmore tr { display: none; }
    table.loadmore tr.active { display: table-row; }
  </style>
@endpush

@section('content')
<?php if(isset($section_1_body) and $section_1_body->content_cover != NULL){?>
<section class="masthead pb-0" style="background-color:#132B50;background-image:url(data:image/jpeg;base64,{{base64_encode($section_1_body->content_cover)}});background-repeat:no-repeat;background-size:cover; align-self: stretch;">
</section>
<?php } else {?>
<section class="masthead pb-0" style="background-color:#132B50;background-repeat:no-repeat;background-size:cover; "></section>
<?php }?>

<?php if(isset($section_1_body)){?>
<section class="py-4 mb-5" style="background-color:#132B50;">
  <div class="container">
      <h2 class="text-left font-rubik fw-bold color-gold fw-bold" style="margin-bottom:0px">{!! nl2br($section_1_body->content_title) !!}</h2>
  </div>
</section>
<?php }?>

<?php if(isset($section_2_header)){?>
<section class="mb-5">
  <div class="container" data-aos="fade-down">
      <h2 class="text-center mb-2 font-rubik fw-bold color-navy fw-bold">{!! nl2br($section_2_header->section_title) !!}</h2>
  </div>
</section>
<?php }?>

<?php if($section_2_body){?>
<?php foreach($section_2_body as $key=>$sb){?>
<section class="mb-5" style="overflow:hidden">
      <div class="container">
          <div class="row align-items-center" data-aos="fade-bottom">
              <?php if($key % 2 == 0){?>
                <div class="col-md-6 mb-3">
                  <div class="float-start" data-aos="fade-right">
                    <?php if($sb->content_cover != NULL){?>
                    <img src="data:image/jpeg;base64,{{base64_encode($sb->content_cover)}}" class="img-fluid">
                    <?php }?>
                  </div>
                </div>
                <div class="col-md-6 text-left" data-aos="fade-left">
                  <p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
                  <p class="font-inter fw-normal fs-16 color-grey"><?=$sb->content_description?></p>
                </div>
              <?php } else if($key % 2 == 1){?>
              <div class="col-md-6 mb-3 text-left" data-aos="fade-right">
                <p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
                <p class="font-inter fw-normal fs-16 color-grey"><?=$sb->content_description?></p>
              </div>
              <div class="col-md-6" data-aos="fade-left">
                <div class="float-end">
                  <?php if($sb->content_cover != NULL){?>
                  <img src="data:image/jpeg;base64,{{base64_encode($sb->content_cover)}}" class="img-fluid">
                  <?php }?>
                </div>
              </div>
              <?php }?>
          </div>
      </div>
</section>
<?php }?>
<?php }?>

<?php if(isset($section_3_header)){?>
<section class="page-section text-white" style="overflow:hidden">
  <div class="container justify-content-center">
    <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! nl2br($section_3_header->section_title) !!}</h2>
    <p class="text-center mb-5 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! nl2br($section_3_header->section_subtitle) !!}</p>
    <div class="row justify-content-center">
      <div class="col-lg-12 mb-lg-0 text-center">
        <ul id="pilar" style="padding-left: 0;">
          <?php if(isset($section_3_body)){?>
          <?php foreach($section_3_body as $sb3){?>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="data:image/jpeg;base64,{{base64_encode($sb3->content_cover)}}">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2"><?=$sb3->content_title?><br><?=$sb3->content_subtitle?></p>
              </div>
          </li>
          <?php }?>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php }?>

<section class="page-section"  style="background:#F1F5F9">
  <div class="container">
      <div class="row">
        <div class="col-md-10">
          <h2 class="mb-2 font-rubik fw-bold color-navy">{!! isset($section_4_header) ? nl2br($section_4_header->section_title) : '' !!}</h2>
          <!--<p class="mb-4 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_4_header) ? nl2br($section_4_header->section_subtitle) : '' !!}</p>-->
        </div>
        <div class="col-md-2 mb-3">
          <!--<select onchange="changeCapaian()" class="form-select form-select-lg br-20 color-dark-navy fs-14" aria-label="">
            <?php for($i=2019;$i<=date('Y');$i++){?>
            <option <?=(isset($search_year) and $i==$search_year) ? 'selected="selected"' : ''?> value="<?=$i?>"><?=$i?></option>
            <?php }?>
          </select>-->
        </div>
        <div class="col-md-12 text-left">
          <div class="table-responsive">
            <?php if($capaian_ipkn){?>
              <table class="table loadmore custom table-bordered table-striped capaian" width="100%">
								<thead>
									<tr>
										<td class="text-center" style="width:30px"><b>No</b></td>
										<td class="text-center" style="width:100px"><b>Provinsi</b></td>
										<td class="text-center" style="width:100px"><b>Ranking</b></td>
										<!-- <td class="text-center" style="width:100px"><b>IPKN</b></td> -->
										<td class="text-center" style="width:100px"><b>Enabling Environment</b></td>
										<td class="text-center" style="width:100px"><b>T&amp;T Policy and Conditions</b></td>
										<td class="text-center" style="width:100px"><b>Infrastructure</b></td>
										<td class="text-center" style="width:100px"><b>T&amp;T Demand Drivers</b></td>
										<td class="text-center" style="width:100px"><b>T&amp;T Sustainability</b></td>
									</tr>
								</thead>
                <tbody>
                  <?php $no=1;foreach($capaian_ipkn as $ci){?>
									<tr>
										<td class="text-center"><?=$no++?></td>
										<td class="text-left"><?=$ci->name?></td>
										<td class="text-center"><?=number_format($ci->rank)?></td>
										<!-- <td class="text-center"><?=$ci->score_ipkn?></td> -->
										<td class="text-center" <?=(get_capaian_ipkn_high_score($ci->province_id)!=NULL and get_capaian_ipkn_high_score($ci->province_id) == "index_1") ? 'style="background: #EAC170;"' : ''?>><?=$ci->index_1?></td>
										<td class="text-center" <?=(get_capaian_ipkn_high_score($ci->province_id)!=NULL and get_capaian_ipkn_high_score($ci->province_id) == "index_2") ? 'style="background: #EAC170;"' : ''?>><?=$ci->index_2?></td>
										<td class="text-center" <?=(get_capaian_ipkn_high_score($ci->province_id)!=NULL and get_capaian_ipkn_high_score($ci->province_id) == "index_3") ? 'style="background: #EAC170;"' : ''?>><?=$ci->index_3?></td>
										<td class="text-center" <?=(get_capaian_ipkn_high_score($ci->province_id)!=NULL and get_capaian_ipkn_high_score($ci->province_id) == "index_4") ? 'style="background: #EAC170;"' : ''?>><?=$ci->index_4?></td>
										<td class="text-center" <?=(get_capaian_ipkn_high_score($ci->province_id)!=NULL and get_capaian_ipkn_high_score($ci->province_id) == "index_5") ? 'style="background: #EAC170;"' : ''?>><?=$ci->index_5?></td>
									</tr>
                  <?php }?>
                </tbody>
              </table>
            <?php }?>
          </div>
        </div>
      </div>
  </div>
  <div class="container text-center">            
    <a id="load_more" class="btn btn-lg fw-bold font-rubik color-navy fs-14 align-items-center" style="background:#fff;border-color: #64748B;border-radius:50px;">Lihat Lebih Banyak</a>
  </div>
</section>

<section class="page-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-5">
        <h2 class="font-rubik fw-bold color-navy">{!! isset($section_5_header) ? nl2br($section_5_header->section_title) : '' !!}</h2>
        <select id="capaian" class="form-select form-select-lg br-20 color-dark-navy fs-14 font-rubik fw-bold" style="width:80%;" onchange="capaianIPKN()" aria-label="">
          <option value="">Semua Provinsi</option>
          <?php if($province){?>
          <?php foreach($province as $p){?>
          <option value="<?=$p->province_id?>"><?=$p->province_name?></option>
          <?php }?>
          <?php }?>
        </select>
      </div>
      <div class="col-md-7">
        <div id="chartIPKN" style="width: 100%;height:400px;font-size:10px"></div>
      </div>
    </div>
  </div>
</section>

<section class="text-white mb-5">
  <div class="container">
      <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! isset($section_6_header) ? nl2br($section_6_header->section_title) : '' !!}</h2>
      <center>
        <!-- <select id="index" class="form-select form-select-lg br-20 color-dark-navy fs-14 mb-3 font-rubik fw-bold" style="width:10%" onchange="indexIPKN()" aria-label="">
          <?php if($year){?>
          <?php foreach($year as $y){?>
          <option <?=($y->dh_year==date('Y')) ? 'selected="selected"' : ''?> value="<?=$y->dh_year?>"><?=$y->dh_year?></option>
          <?php }?>
          <?php }?>
        </select> -->
      </center>
      <div class="table-responsive" id="table_index">
        @include('ipkn.index')
      </div>
  </div>
</section>

<!-- modal-->
<div class="modal fade" id="modal-content-view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color:#000" id="text-title-modal">Information</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="modal-content-body-load-page"></div>
            </div>
        </div>
    </div>
</div>
<!--modal-->
@endsection

@push('script')
<script src="<?=asset('js/jquery.min.js')?>"></script>
<script src="<?=asset('js/amcharts/core.js')?>"></script>
<script src="<?=asset('js/amcharts/charts.js')?>"></script>
<script src="<?=asset('js/aos.js')?>"></script>
<script>
  $(function() {
    AOS.init();
    var chart = am4core.create("chartIPKN", am4charts.RadarChart);

    chart.dataSource.url = "{{route('ipkn/capaian')}}";
    chart.dataSource.parser = new am4core.JSONParser;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "pillar";

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    /* Create and configure series */
    var series = chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "pillar";
    series.name = "";
    series.strokeWidth = 2;
    series.bullets.push(new am4charts.CircleBullet());

    var circleBullet = series.bullets.push(new am4charts.CircleBullet());
    circleBullet.tooltipText = "Value: [bold]{value}[/]";
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{value}";
    labelBullet.label.dy = -20;

    chart.cursor= new am4charts.RadarCursor();
  });
  
  function capaianIPKN(){
    var chart = am4core.create("chartIPKN", am4charts.RadarChart);

    chart.dataSource.url = "{{route('ipkn/capaian')}}?id="+$("#capaian").val();
    chart.dataSource.parser = new am4core.JSONParser;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "pillar";

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    /* Create and configure series */
    var series = chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "pillar";
    series.name = "";
    series.strokeWidth = 2;
    series.bullets.push(new am4charts.CircleBullet());

    var circleBullet = series.bullets.push(new am4charts.CircleBullet());
    circleBullet.tooltipText = "Value: [bold]{value}[/]";
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{value}";
    labelBullet.label.dy = -20;

    chart.cursor= new am4charts.RadarCursor();
  }
</script>
<script type="text/javascript">
  $(".fold-table tr.view").on("click", function(){
      $(this).toggleClass("open").next(".fold").toggleClass("open");
  });

  function show_modal(id){
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      url: "{{ route('ipkn/information') }}",
      data: {id: id}
    }).done(function(response) {
      $('#modal-content-body-load-page').html(response);
    });
    $("#modal-content-view").modal('show');
  }

  function indexIPKN(){
    var year = $("#index").val();
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "GET",
      url: "{{ route('ipkn/index') }}?year="+year,
    }).done(function(response) {
      $('#table_index').html(response);
    });
  }

  $('table tr:nth-child(n+1):nth-child(-n+5)').addClass('active');
  $('#load_more').on('click', function(e) {console.log('das');
    e.preventDefault();  
    var $rows = $('.capaian tr');
    var lastActiveIndex = $rows.filter('.active:last').index();
    $rows.filter(':lt(' + (lastActiveIndex + 7) + ')').addClass('active');
  });
</script>
@endpush
