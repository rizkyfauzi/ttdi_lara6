<table class="table custom table-striped fold-table">
    <thead>
        <tr style="background: linear-gradient(90deg, #2F5B9E 0%, #132B50 100%);color:#fff">
            <th style="width: 60px; text-align: center;">No</th>
            <th>Index Component</th>
            <th style="width: 165px; text-align: right">Score <?=($last_year_index - 2)?> (TTCI)</th>
            <th style="width: 165px; text-align: right">Score <?=$last_year_index?> (TTDI)</th>
            <th style="width: 30px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr class="view">
            <?php
                $last_score = get_ttdi_global_index('last_score',2019);
                $current_score = get_ttdi_global_index('score',2021);

                $icon = "";
                if($last_score > $current_score){
                    $icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
                } else if($last_score < $current_score){
                    $icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
                } else {
                    $icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
                }
            ?>
            <td style="text-align: center"></td>
            <td style="text-align: left; font-weight: bold; font-size: 16px">Travel & Tourism Development Index</td>
            <td style="width:165px; text-align: right"><?=$last_score?></td>
            <td style="width:165px; text-align: right"><?=$current_score?> &nbsp; <?=$icon?></td>
            <td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
        </tr>

        <?php if($index){?>
        <?php $no=1;foreach($index as $i){?>
        <tr class="view" style="background: #122b5254">
            <?php
                $last_score = get_ttdi_score_index($i->index_id,'last_score',2019);
                $current_score = get_ttdi_score_index($i->index_id,'score',2021);

                $icon = "";
                if($last_score > $current_score){
                    $icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
                } else if($last_score < $current_score){
                    $icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
                } else {
                    $icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
                }
            ?>
            <td style="text-align: center"><?=$no++?></td>
            <td style="text-align: left"><?=$i->index_desc?></td>
            <td style="width:165px; text-align: right"><?=$last_score?> </td>
            <td style="width:165px; text-align: right"><?=$current_score?>  <?=$icon?></td>
            <td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
        </tr>
        <?php $pillar = get_pillar_ttdi($i->index_id);?>
        <?php if($pillar){?>
        <?php foreach($pillar as $p){?>
            <tr class="view">
                <td style="text-align: center"></td>
                <td style="text-align: left">
                    <img src="https://ttci.kemenparekraf.go.id/uploaded/images/<?=$p->pillar_icon?>" width="40px"> <?=$p->pillar_desc.' '.$p->pillar_note?>
                </td>
                <td style="width:165px; text-align: right"><?=get_ttdi_score_pillar($p->pillar_id,'last_score',2019)?></td>
                <td style="width:165px; text-align: right">
                    <?=get_ttdi_score_pillar($p->pillar_id,'score',2021)?> 
                    <?php
                        $last_score = get_ttdi_score_pillar($p->pillar_id,'last_score',2019);
                        $current_score = get_ttdi_score_pillar($p->pillar_id,'score',2021);

                        $icon = "";
                        if($last_score > $current_score){
                            $icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
                        } else if($last_score < $current_score){
                            $icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
                        } else {
                            $icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
                        }
                    ?>
                    <?=$icon?>
                </td>
                <td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
            </tr>

            <tr class="fold">
                <td class="fold-area" colspan="5">
                    <div class="fold-content">
                        <table class="table custom">
                            <tbody>
                                <?php $sub_pillar = get_sub_pillar_ttdi($p->pillar_id)?>
                                <?php if($sub_pillar){?>
                                <?php foreach($sub_pillar as $sp){?>
                                <tr>
                                    <td style="width: 60px"></td>
                                    <td style="text-align: left;">
                                        <a class="info" style="cursor:pointer !important" onclick="show_modal(<?=$sp->subpillar_id?>)">
                                            <i class="fa fa-circle-info color-grey fs-20 color-grey fs-20"></i>
                                        </a> <?=$sp->subpillar_desc?>
                                    </td>
                                    <?php
                                        $last_score = get_ttdi_score_sub_pillar($p->pillar_id,$sp->subpillar_id,'last_score',2019);
                                        $current_score = get_ttdi_score_sub_pillar($p->pillar_id,$sp->subpillar_id,'score',2021);

                                        $icon = "";
                                        if($last_score > $current_score){
                                            $icon .= '<span class="arrow-down-color" style="color:red"><i class="fa fa-arrow-down"></i></span>';
                                        } else if($last_score < $current_score){
                                            $icon .= '<span class="arrow-up-color" style="color:green"><i class="fa fa-arrow-up"></i></span>';
                                        } else {
                                            $icon .= '<span class="arrow-up-color" style="color:grey"><i class="fa fa-minus"></i></span>';
                                        }
                                    ?>
                                    <td style="width:165px; text-align: right"><?=$last_score?></td>
                                    <td style="width:165px; text-align: right"><?=$current_score?> 
                                        <?=$icon?>
                                    </td>
                                    <th style="width:30px; text-align: right">&nbsp;</th>
                                </tr>
                                <?php }?>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        <?php }?>
        <?php }?>
        <?php }?>
        <?php }?>
    </tbody>
</table>