@extends('app')
@push('style')
<link href="<?=asset('css/aos.css')?>" rel="stylesheet">
@endpush

@section('content')

<?php
  // default year
  $default_year = 2021;
?>
<?php if(isset($section_1_body) and $section_1_body->content_cover != NULL){?>
<section class="masthead pb-0" style="background-color:#132B50;background-image:url(data:image/jpeg;base64,{{base64_encode($section_1_body->content_cover)}});background-repeat:no-repeat;background-size:cover;"></section>
<?php } else {?>
<section class="masthead pb-0" style="background-color:#132B50;background-repeat:no-repeat;background-size:cover; "></section>
<?php }?>

<?php if(isset($section_1_body)){?>
<section class="py-4 mb-5" style="background-color:#132B50;">
  <div class="container">
    <h2 class="text-left font-rubik fw-bold color-gold fw-bold" style="margin-bottom:0px">{!! nl2br($section_1_body->content_title) !!}</h2>
  </div>
</section>
<?php }?>

<?php if(isset($section_2_header)){?>
<section class="mb-5">
  <div class="container" data-aos="fade-down">
    <h2 class="text-center mb-2 font-rubik fw-bold color-navy fw-bold">{!! nl2br($section_2_header->section_title) !!}</h2>
    <p class="text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1" style="margin-bottom:0px">{!! nl2br($section_2_header->section_subtitle) !!}</p>
  </div>
</section>
<?php }?>

<?php if(isset($section_2_body)){?>
<section class="text-white mb-5" style="overflow:hidden">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6 text-left" data-aos="fade-right">
        <p class="font-inter fw-normal-2 fs-20 color-dark-navy">{!! nl2br($section_2_body->content_title)  !!}</p>
        <div class="font-inter fw-normal fs-16 color-grey">{!! nl2br($section_2_body->content_description) !!}</div>
      </div>
      <div class="col-md-6" data-aos="fade-left">
        <?php if(isset($section_2_body) and $section_2_body->content_cover != NULL){?>
        <img src="data:image/jpeg;base64,{{base64_encode($section_2_body->content_cover)}}" class="img-fluid float-end">
        <?php }?>
      </div>
    </div>
  </div>
</section>
<?php }?>


<?php if(isset($section_4_header)){?>
<section class="page-section text-white" style="overflow:hidden">
  <div class="container justify-content-center">
    <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! nl2br($section_4_header->section_title) !!}</h2>
    <p class="text-center mb-5 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! nl2br($section_4_header->section_subtitle) !!}</p>
    <div class="row justify-content-center">
      <div class="col-lg-12 mb-lg-0 text-center">
        <ul id="pilar" style="padding-left: 0;">
          <?php if(isset($section_4_body)){?>
          <?php foreach($section_4_body as $sb4){?>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="data:image/jpeg;base64,{{base64_encode($sb4->content_cover)}}">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2"><?=$sb4->content_title?><br><?=$sb4->content_subtitle?></p>
              </div>
          </li>
          <?php }?>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php }?>

<section class="page-section" style="background:#F1F5F9">
  <div class="container">
      <div class="row align-items-center">
        <div class="col-md-10">
          <h2 class="mb-2 font-rubik fw-bold color-navy">{!! isset($section_5_header) ? nl2br($section_5_header->section_title) : '' !!}</h2>
          <p class="mb-4 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_5_header) ? nl2br($section_5_header->section_subtitle) : 
            '' !!}</p>
        </div>
        <div class="col-md-2 mb-3">
          <select id="capaian_table" class="form-select form-select-lg br-20 color-dark-navy fs-14 mb-3 font-rubik fw-bold" onchange="tabelCapaianTTDI()" aria-label="">
            <?php if($table_year){?>
            <?php foreach($table_year as $ty){?>
            <option <?=($default_year==$default_year) ? 'selected="selected"' : ''?> value="<?=$default_year?>"><?=$default_year?></option>
            <?php }?>
            <?php }?>
          </select>
        </div>
        <div class="table-responsive" id="table_capaian">
          <?php if($capaian_ttdi){?>
            <table class="table custom table-bordered table-striped table-responsive" width="100%">
              <thead>
                <tr>
                  <td style="background: #EAC170;"><b style="font-size:20px">Sub-Index</b></td>
                  <?php foreach($capaian_ttdi as $ct){?>
                  <td class="text-center" style="width:100px"><b><?=$ct->country_name?></b></td>
                  <?php }?>
                </tr>
              </thead>
              <tbody>
                  <?php if($sub_index){?>
                  <?php foreach($sub_index as $key=>$si){?>
                  <?php if($key < 5){?>
                  <?php $index_id = $key+1?>
                  <tr>
										<td><b><?=$si->index_desc?></b></td>
                    <?php foreach($capaian_ttdi as $ct){?>
                    <?php $ct_index = "index_".$index_id?>
										<td class="text-center" <?=(get_capaian_ttdi_high_score('index_'.$index_id,$last_year->year)!=NULL and get_capaian_ttdi_high_score('index_'.$index_id,$last_year->year) == $ct->$ct_index) ? 'style="background: #EAC170;"' : ''?>><?=$ct->$ct_index?></td>
                    <?php }?>
									</tr>
                  <?php }?>
                  <?php }?>
                  <?php }?>
              </tbody>
            </table>
          <?php }?>
        </div>
      </div>
  </div>
</section>

<section class="page-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-5">
        <h2 class="font-rubik fw-bold color-navy">{!! isset($section_6_header) ? nl2br($section_6_header->section_title) : '' !!}</h2>
        <select id="capaian" class="form-select form-select-lg br-20 color-dark-navy fs-14 font-rubik fw-bold" style="width:30%" onchange="capaianTTDI()" aria-label="">
            <?php if($year){?>
            <?php foreach($year as $y){?>
            <option <?=($y->year==$default_year) ? 'selected="selected"' : ''?> value="<?=$y->year?>"><?=$y->year?></option>
            <?php }?>
            <?php }?>
        </select>
      </div>
      <div class="col-md-7">
        <div id="chartTTDI" style="width: 100%;height:400px;font-size:10px"></div>
      </div>
    </div>
  </div>
</section>

<section class="text-white mb-5">
  <div class="container">
        <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! isset($section_7_header) ? nl2br($section_7_header->section_title) : '' !!}</h2>
				<p class="fst-italic text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_7_header) ? nl2br($section_7_header->section_subtitle) : '' !!}</p>
				<center>
          <select id="index" class="form-select form-select-lg br-20 color-dark-navy fs-14 mb-3 font-rubik fw-bold" style="width:10%" onchange="indexTTDI()" aria-label="">
            <?php if($year){?>
            <?php foreach($year as $y){?>
            <option <?=($y->year==$default_year) ? 'selected="selected"' : ''?> value="<?=$y->year?>"><?=$y->year?></option>
            <?php }?>
            <?php }?>
          </select>
        </center>
				<div class="table-responsive" id="table_index">
          @include('ttdi.index')
        </div>
  </div>
</section>

<!-- modal-->
<div class="modal fade" id="modal-content-view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color:#000" id="text-title-modal">Information</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!-- modal information-->
            <div class="modal-body">
                <div id="modal-content-body-load-page"></div>
            </div>
            <!-- modal information-->
        </div>
    </div>
</div>
<!--modal-->
@endsection

@push('script')
<script src="<?=asset('js/jquery.min.js')?>"></script>
<script src="<?=asset('js/amcharts/core.js')?>"></script>
<script src="<?=asset('js/amcharts/charts.js')?>"></script>
<script src="<?=asset('js/aos.js')?>"></script>
<script>
  $(function() {
    AOS.init();
		var chart = am4core.create("chartTTDI", am4charts.RadarChart);
		
		chart.dataSource.url = "{{route('ttdi/capaian')}}";
    chart.dataSource.parser = new am4core.JSONParser;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "pillar";

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    var series = chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "pillar";
    series.name = "";
    series.strokeWidth = 2;
    series.bullets.push(new am4charts.CircleBullet());

    var circleBullet = series.bullets.push(new am4charts.CircleBullet());
    circleBullet.tooltipText = "Value: [bold]{value}[/]";
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{value}";
    labelBullet.label.dy = -20;

    chart.cursor= new am4charts.RadarCursor();
  });

  function capaianTTDI(){
    var chart = am4core.create("chartTTDI", am4charts.RadarChart);

    chart.dataSource.url = "{{route('ttdi/capaian')}}?year="+$("#capaian").val();
    chart.dataSource.parser = new am4core.JSONParser;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "pillar";

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    var series = chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "pillar";
    series.name = "";
    series.strokeWidth = 2;
    series.bullets.push(new am4charts.CircleBullet());

    var circleBullet = series.bullets.push(new am4charts.CircleBullet());
    circleBullet.tooltipText = "Value: [bold]{value}[/]";
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{value}";
    labelBullet.label.dy = -20;

    chart.cursor= new am4charts.RadarCursor();
  }
</script>
<script type="text/javascript">
  $(".fold-table tr.view").on("click", function(){
      $(this).toggleClass("open").next(".fold").toggleClass("open");
  });

  function show_modal(id){
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      url: "{{ route('ttdi/information') }}",
      data: {id: id}
    }).done(function(response) {
      $('#modal-content-body-load-page').html(response);
    });
    $("#modal-content-view").modal('show');
  }

  function indexTTDI(){
    var year = $("#index").val();
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "GET",
      url: "{{ route('ttdi/index') }}?year="+year,
    }).done(function(response) {
      $('#table_index').html(response);
    });
  }

  function tabelCapaianTTDI(){
    var year = $("#capaian_table").val();
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "GET",
      url: "{{ route('ttdi/table') }}?year="+year,
    }).done(function(response) {
      $('#table_capaian').html(response);
    });
  }
</script>
@endpush
