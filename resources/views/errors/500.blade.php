@extends('errors::minimal')

@section('title', __('Server Error'))
@section('content')
<section style="padding-top: calc(6rem + 64px);">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 text-center">
                <img src="<?=asset('assets/img/error-page/500.png')?>" class="img-fluid" style="height:350px">
            </div>
            <div class="col-md-6 show-padding">
                <h2 class="text-left mb-2 ttdi-1">500</h2>
                <h2 class="text-left mb-2 ttdi-1">Halaman Bermasalah</h2>
                <p class="text-left mb-3 ttdi-1 title-2 fw-normal-1">Silahkan coba muat kembali halaman ini atau kembali lagi beberapa saat ke depan.</p>
                <a class="btn btn-lg" href="#" style="background:#F1F5F9;font-weight:600;font-family:'Rubik';color: #132B50;border-color: #F1F5F9;border-radius:20px;font-size:14px;"><i class="fa fa-arrow-left"></i> Kembali ke Halaman Utama</a>
            </div>
        </div>
    </div>
</section>
@endsection
