<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>@yield('title')</title>
        <!-- Favicon-->
        <link rel="icon" type="image/png" href="<?=asset('assets/img/favicon-32x32.png')?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?=asset('css/styles.css')?>" rel="stylesheet" />
    </head>
    <body id="page-top">
		
		<nav class="navbar navbar-expand-lg bg-logo fixed-top" id="mainNav">
            <div class="container">
				<a class="logo mobile" href="{{ route('beranda') }}">
					<img src="<?=asset('assets/img/kemenparekraf-logo.png')?>" height="56">
				</a>
				<a class="btn navbar-brand fs-14 font-rubik fw-normal-1 mobile" href="<?=env('BACKEND_URL')?>" style="border:1.5px solid #EAC170;color:#EAC170;border-radius:20px;">Login</a>
            </div>
        </nav>
		
		<!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-menu fixed-top top86 login" id="mainNav">
            <div class="container">
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				  <i class="fas fa-bars color-gold-home"></i>
				</button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('beranda') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'beranda' or Request::segment(1) == "") ? 'active' : ''}}" ><i class="fa fa-house color-grey"></i> Beranda</a>
						</li>
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('ttdi') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'ttdi') ? 'active' : ''}}" ><i class="fa fa-globe color-grey"></i> TTDI</a>
						</li>
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('ipkn') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'ipkn') ? 'active' : ''}}" ><i class="fa fa-map-location-dot color-grey"></i> IPKN</a>
						</li>
						<li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('data') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'data') ? 'active' : ''}}" ><i class="fa fa-signal color-grey"></i> Data</a>
						</li>
                        <li class="nav-item mx-0 mx-lg-1">
							<a href="{{ route('berita') }}" class="nav-link font-rubik py-2 px-0 px-lg-3 rounded {{ (Request::segment(1) == 'berita') ? 'active' : ''}}" ><i class="fa fa-newspaper color-grey"></i> Berita</a>
						</li>
                    </ul>
                </div>
				<a class="text-center align-items-center login" href="{{ route('beranda') }}"><img src="<?=asset('assets/img/logo_kemenparekraf_warna.png')?>" height="40"></a>
				<a class="navbar-brand fs-14 login font-rubik fw-normal-1" style="color:#EAC170;" href="<?=env('BACKEND_URL')?>">Login</a>
            </div>
        </nav>

        @yield('content')
		
		<!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
