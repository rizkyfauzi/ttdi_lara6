@extends('app')
@push('style')
<link href="<?=asset('css/aos.css')?>" rel="stylesheet">
@endpush

@section('content')
		<?php if(isset($section_banner_body) and $section_banner_body->content_cover != NULL){?>
		<section class="mb-5" style="padding-top: calc(6rem + 32px);overflow:hidden;background-image:url(data:image/jpeg;base64,{{base64_encode($section_banner_body->content_cover)}}) ;background-position:right;background-repeat:no-repeat;height:640px;background-color:#132B50;">
			<div class="container text-left py-5 ps-4">
				<div class="row">
					<div class="col-md-3">
						<ul id="social-media" style="padding-left: 0;">
							<?php 
								$logo = get_image_content($section_banner_body->content_id,2);
								if($logo){
								foreach($logo as $l){
							?>
							<li class="mb-2"><img src="data:image/jpeg;base64,{{base64_encode($l->file_content)}}" style="vertical-align:bottom" width="80px"></li>
							<?php }?>
							<?php }?>
						</ul>
						<p class="font-rubik fw-normal fs-16 text-white">{!! nl2br($section_banner_body->content_title) !!}</p>
						<h1 class="font-rubik fw-normal-2 fs-36 color-gold">{!! str_replace(" ","<br>",$section_banner_body->content_subtitle) !!}</h1>
					</div>
				</div>
			</div>
		</section>
		<?php } else {?>
		<section class="mb-5" style="padding-top: calc(6rem + 32px);overflow:hidden;background-position:right;background-repeat:no-repeat;height:640px;background-color:#132B50;">
			<div class="container text-left py-5 ps-4">
				<div class="row">
					<div class="col-md-6">
						<ul id="social-media" style="padding-left: 0;">
							<?php 
								$logo = get_image_content($section_banner_body->content_id,2);
								if($logo){
								foreach($logo as $l){
							?>
							<li class="mb-2"><img src="data:image/jpeg;base64,{{base64_encode($l->file_content)}}" style="vertical-align:bottom" width="80px"></li>
							<?php }?>
							<?php }?>
						</ul>
						<p class="font-rubik fw-normal fs-16 text-white">{!! (isset($section_banner_body)) ? nl2br($section_banner_body->content_title) : '' !!}</p>
						<h1 class="font-rubik fw-normal-2 fs-36 color-gold">{!! (isset($section_banner_body)) ? str_replace(" ","<br>",$section_banner_body->content_subtitle) : '' !!}</h1>
					</div>
				</div>
			</div>
		</section>
		<?php }?>
		
		<?php if(isset($section_1_header) and $section_1_header->section_title != NULL){?>
		<section class="mb-5">
            <div class="container">
                <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! nl2br($section_1_header->section_title) !!}</h2>
				<p class="text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1 mb-6">{!! nl2br($section_1_header->section_subtitle) !!}</p>
			</div>
		</section>
		<?php }?>
		
		<?php if(isset($section_1_body) and count($section_1_body) > 0){?>
		<?php foreach($section_1_body as $key=>$sb){?>
		<section class="mb-5">
            <div class="container">
                <div class="row align-items-center">
					<?php if($key % 2 == 0){?>
					<div class="col-md-7 text-left">
						<p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
						<p class="font-inter fw-normal fs-16 color-grey mb-5"><?=$sb->content_description?></p>
					</div>
					<div class="col-md-5">
						<div class="row float-end mb-5" style="margin-top:30px">
							<?php if(get_image_content($sb->content_id,2)){?>
							<?php foreach(get_image_content($sb->content_id,2) as $k=>$r){?>
								<?php if($r->file_content != NULL){?>
								<div class="col <?=($k == 0) ? 'mb-3' : ''?>">
									<img src="<?=asset('assets/img/rectangle-big.png')?>" data-real-src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="lazy img-fluid" <?=($k == 0) ? ' style="margin-top:-30px"' : ''?>>
								</div>
							<?php }?>
							<?php }?>
							<?php }?>
						</div>
					</div>
					<?php } else if($key % 2 == 1){?>
					<div class="col-md-5">
						<div class="row float-start mb-3">
							<?php if(get_image_content($sb->content_id,2)){?>
							<?php foreach(get_image_content($sb->content_id,2) as $k=>$r){?>
							<?php if($r->file_content != NULL){?>
							<div class="col <?=($k == 0) ? 'mb-3' : ''?>">
								<img src="<?=asset('assets/img/rectangle-big.png')?>" data-real-src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="lazy img-fluid" <?=($k == 1) ? 'style="margin-top:-30px"' : ''?>>
							</div>
							<?php }?>
							<?php }?>
							<?php }?>
						</div>
					</div>
					<div class="col-md-7 text-left">
						<p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
						<p class="font-inter fw-normal fs-16 color-grey"><?=$sb->content_description?></p>
					</div>
					<?php }?>
				</div>
			</div>
		</section>
		<?php }?>
		<?php }?>		
		
		<!-- Transformasi Section-->
		<?php if(isset($section_2_header)){?>
        <section class="page-section" style="background:#F1F5F9;overflow:hidden">
            <div class="container">
                <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! nl2br($section_2_header->section_title) !!}</h2>
				<p class="fst-italic text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1 mb-6">{!! nl2br($section_2_header->section_subtitle) !!}</p>
                <div class="show-big-banner">
					<div class="row justify-content-center mb-5">
						<?php if(get_image_content(6,4)){?>
						<?php foreach(get_image_content(6,4) as $k=>$r){?>
						<?php if($r->file_content != NULL){?>
						<div class="col-lg-2"><img src="<?=asset('assets/img/rectangle.png')?>" data-real-src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="lazy img-fluid w-100"></div>
						<?php }?>
						<?php }?>
						<?php }?>
						<?php if(isset($section_2_body) and $section_2_body->content_cover != NULL){?>
						<div class="col-lg-3"><img src="<?=asset('assets/img/rectangle.png')?>" data-real-src="data:image/jpeg;base64,{{base64_encode($section_2_body->content_cover)}}" class="lazy img-fluid w-100" style="margin-top:-72px"></div>
						<?php }?>
					</div>
                </div>
				<div class="hide-big-banner">
					<div class="row mb-3">
						<?php if(get_image_content(6,4)){?>
						<?php foreach(get_image_content(6,4) as $k=>$r){?>
						<?php if($r->file_content != NULL){?>
						<div class="col <?=($k == 0 or $k == 1) ? 'mb-4' : 'mb-2'?>"><img src="<?=asset('assets/img/rectangle-big.png')?>" data-real-src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="lazy img-fluid w-100"></div>
						<?php }?>
						<?php }?>
						<?php }?>
					</div>
					<div class="row mb-5 justify-content-center">
						<div class="col-md-5 text-center">
							<?php if(isset($section_2_body) and $section_2_body->content_cover != NULL){?>
							<img src="<?=asset('assets/img/rectangle-big.png')?>" data-real-src="data:image/jpeg;base64,{{base64_encode($section_2_body->content_cover)}}" class="lazy img-fluid w-100">
							<?php }?>
						</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-lg-8" data-aos="fade-bottom">
						<h6 class="text-center mb-3 font-inter fs-20 fw-normal-2 font-rubik fw-bold color-navy"><?=isset($section_2_body) ? $section_2_body->content_title : ''?></h6>
						<div class="lead"><?=isset($section_2_body) ? $section_2_body->content_description : ''?></div>
						<center>
						<a class="btn btn-lg custom_btn font-rubik fw-normal-1 color-gold fs-14" href="{{ route('ttdi') }}" style="background:#1E293B;border-radius:50px;">Selengkapnya&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>
						</center>
					</div>
                </div>
            </div>
        </section>
		<?php }?>
		
		<?php if(isset($section_3_header)){?>
		<section class="page-section">
            <div class="container justify-content-center">
                <div class="row align-items-center">
					<div class="col-md-5">
						<h2 class="font-rubik fw-bold color-navy">{!! nl2br($section_3_header->section_title) !!}</h2>
						<select id="capaianTTDI" class="form-select form-select-lg br-20 color-dark-navy fs-14 font-rubik fw-bold" style="width:30%" onchange="capaianTTDI()" aria-label="">
							<?php if($year_ttdi){?>
							<?php foreach($year_ttdi as $y){?>
							<option <?=($y->year==date('Y')) ? 'selected="selected"' : ''?> value="<?=$y->year?>"><?=$y->year?></option>
							<?php }?>
							<?php }?>
						</select>
					</div>
					<div class="col-md-7">
						<div id="chartTTDI" style="width: 100%;height:400px;font-size:10px"></div>
					</div>
				</div>
			</div>
		</section>
		<?php }?>
		
		<?php if(isset($section_4_body)){?>
		<section class="page-section"  style="background:#F1F5F9;overflow:hidden">
            <div class="container">
                <div class="row">
					<div class="col-md-6 mb-3" data-aos="fade-right">
						<?php if($section_4_body->content_cover != NULL){?>
						<img src="data:image/jpeg;base64,{{base64_encode($section_4_body->content_cover)}}" class="img-fluid float-end">
						<?php }?>
					</div>
					<div class="col-md-6 text-left" data-aos="fade-left">
						<h2 class="text-left mb-2 font-rubik fw-bold color-navy">{!! nl2br($section_4_body->content_title) !!}</h2>
						<p class="font-inter fs-20 fw-normal-2 color-dark-navy">{!! nl2br($section_4_body->content_subtitle) !!}</p>
						<div class="font-inter fs-16 fw-normal color-grey">{!! $section_4_body->content_description !!}</div>
						<?php if($section_4_body->content_description != NULL){?>
						<a class="btn btn-lg custom_btn font-rubik fw-normal-1 color-gold fs-14" href="{{ route('ipkn') }}" style="background:#1E293B;border-radius:50px;">Selengkapnya&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>
						<?php }?>
                    </a>
					</div>
				</div>
			</div>
		</section>
		<?php }?>
		
		<section class="page-section">
            <div class="container justify-content-center">
                <div class="row align-items-center">
					<div class="col-md-5">
						<h2 class="font-rubik fw-bold color-navy">{!! isset($section_5_header) ? nl2br($section_5_header->section_title) : '' !!}</h2>
						<select id="capaianIPKN" class="form-select form-select-lg br-20 color-dark-navy fs-14 font-rubik fw-bold" style="width:80%" onchange="capaianIPKN()" aria-label="">
							<option value="">Semua Provinsi</option>
							<?php if($province){?>
							<?php foreach($province as $p){?>
							<option value="<?=$p->province_id?>"><?=$p->province_name?></option>
							<?php }?>
							<?php }?>
						</select>
					</div>
					<div class="col-md-7">
						<div id="chartIPKN" style="width: 100%;height:400px;font-size:10px"></div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- Berita Section-->
        <section class="text-white mb-5">
            <div class="container">
                <h2 class="text-center mb-2 font-rubik fw-bold color-navy fw-bold">{!! isset($section_6_header) ? nl2br($section_6_header->section_title) : '' !!}</h2>
                <h2 class="text-center mb-5 font-rubik fw-bold color-navy fw-bold">{!! isset($section_6_header) ? nl2br($section_6_header->section_subtitle) : '' !!}</h2>
				@if($berita)
				@foreach ($berita as $val)
				<div class="row">
					<div class="col-md-12">
						<div class="card mb-4" style="display:block">
							<a href="{{ route('berita/detail',[$val->content_id]) }}">
								<div class="row">
									<div class="col-md-4">
										<?php if($val->content_cover != NULL){?>
										<img src="data:image/jpeg;base64,{{base64_encode($val->content_cover)}}" class="padding-img-news" style="overflow:hidden;height:auto;width:100%">
										<?php }?>
									</div>
									<div class="col-md-8">
										<div class="card-body">
											<h5 class="card-title line-clamp-3 color-dark-navy font-inter fs-20 fw-normal-2 mb-3">{{ $val->content_title }}</h5>
											<div class="font-inter fs-16 line-clamp-3 color-grey">{!! short_desc($val->content_description) !!}</div>
											<div class="row align-items-end" style="height:4.3rem">
												<div class="col">
													<table class="table table-borderless" style="margin-bottom:0">
														<tr>
															<td rowspan="2" class="text-left" style="vertical-align:middle;width:60px">
																<img src="assets/img/logo_kemenparekraf_warna.png" height="50">
															</td>
															<td colspan="2">
																<small class="font-inter color-dark-navy fw-normal-2 fs-14 mb-1">{{ $val->content_owner }}</small>
															</td>
															<td rowspan="2" style="vertical-align:middle;text-align:right">
																<small class="font-inter fw-normal-1 fs-14 color-dark-navy mb-1"><i class="fa fa-eye"></i> {{ $val->content_view_count }}</small>
															</td>
														</tr>
														<tr>
															<td>
																<small class="font-inter color-grey fw-normal fs-12">{{ format_tanggal($val->content_publish_date) }}</small>
															</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				@endforeach
				@endif
			</div>
			<?php if(count($berita) > 0){?>
			<div class="container text-center">
				<a class="btn btn-lg fw-bold font-rubik color-navy fs-14" href="{{ route('berita') }}" style="background:#fff;border-color: #64748B;border-radius:50px;">Cek Berita Lainnya</a>
            </div>
			<?php }?>
        </section>
		
@endsection

@push('script')
<script src="<?=asset('js/jquery.min.js')?>"></script>
<script src="<?=asset('js/jquery.lazy.min.js')?>"></script>
<script src="<?=asset('js/amcharts/core.js')?>"></script>
<script src="<?=asset('js/amcharts/charts.js')?>"></script>
<script src="<?=asset('js/aos.js')?>"></script>
<script>
	
	$('img[src="<?=asset('assets/img/rectangle.png')?>"]').each(function(index, el) {
		$(el).attr('src', $(el).data('real-src'));
	});
	$('img[src="<?=asset('assets/img/rectangle-big.png')?>"]').each(function(index, el) {
		$(el).attr('src', $(el).data('real-src'));
	});

  	$(function() {
		AOS.init();
        $('.lazy').lazy();

		//TTDI
		var chart = am4core.create("chartTTDI", am4charts.RadarChart);
			
		chart.dataSource.url = "{{route('ttdi/capaian')}}";
		chart.dataSource.parser = new am4core.JSONParser;

		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "pillar";

		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
		valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

		var series = chart.series.push(new am4charts.RadarSeries());
		series.dataFields.valueY = "value";
		series.dataFields.categoryX = "pillar";
		series.name = "";
		series.strokeWidth = 2;
		series.bullets.push(new am4charts.CircleBullet());

		var circleBullet = series.bullets.push(new am4charts.CircleBullet());
		circleBullet.tooltipText = "Value: [bold]{value}[/]";
		var labelBullet = series.bullets.push(new am4charts.LabelBullet());
		labelBullet.label.text = "{value}";
		labelBullet.label.dy = -20;

		chart.cursor= new am4charts.RadarCursor();

		//IPKN
		var chart2 = am4core.create("chartIPKN", am4charts.RadarChart);

		chart2.dataSource.url = "{{route('ipkn/capaian')}}";
		chart2.dataSource.parser = new am4core.JSONParser;

		var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "pillar";

		var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.axisFills.template.fill = chart2.colors.getIndex(2);
		valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

		var series = chart2.series.push(new am4charts.RadarSeries());
		series.dataFields.valueY = "value";
		series.dataFields.categoryX = "pillar";
		series.name = "";
		series.strokeWidth = 2;
		series.bullets.push(new am4charts.CircleBullet());

		var circleBullet = series.bullets.push(new am4charts.CircleBullet());
		circleBullet.tooltipText = "Value: [bold]{value}[/]";
		var labelBullet = series.bullets.push(new am4charts.LabelBullet());
		labelBullet.label.text = "{value}";
		labelBullet.label.dy = -20;

		chart2.cursor= new am4charts.RadarCursor();
	});

	function capaianTTDI(){
		var chart = am4core.create("chartTTDI", am4charts.RadarChart);

		chart.dataSource.url = "{{route('ttdi/capaian')}}?year="+$("#capaianTTDI").val();
		chart.dataSource.parser = new am4core.JSONParser;

		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "pillar";

		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
		valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

		var series = chart.series.push(new am4charts.RadarSeries());
		series.dataFields.valueY = "value";
		series.dataFields.categoryX = "pillar";
		series.name = "";
		series.strokeWidth = 2;
		series.bullets.push(new am4charts.CircleBullet());

		var circleBullet = series.bullets.push(new am4charts.CircleBullet());
		circleBullet.tooltipText = "Value: [bold]{value}[/]";
		var labelBullet = series.bullets.push(new am4charts.LabelBullet());
		labelBullet.label.text = "{value}";
		labelBullet.label.dy = -20;

		chart.cursor= new am4charts.RadarCursor();
	}

	function capaianIPKN(){
		var chart2 = am4core.create("chartIPKN", am4charts.RadarChart);

		chart2.dataSource.url = "{{route('ipkn/capaian')}}?id="+$("#capaianIPKN").val();
		chart2.dataSource.parser = new am4core.JSONParser;

		var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "pillar";

		var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.axisFills.template.fill = chart2.colors.getIndex(2);
		valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

		var series = chart2.series.push(new am4charts.RadarSeries());
		series.dataFields.valueY = "value";
		series.dataFields.categoryX = "pillar";
		series.name = "";
		series.strokeWidth = 2;
		series.bullets.push(new am4charts.CircleBullet());

		var circleBullet = series.bullets.push(new am4charts.CircleBullet());
		circleBullet.tooltipText = "Value: [bold]{value}[/]";
		var labelBullet = series.bullets.push(new am4charts.LabelBullet());
		labelBullet.label.text = "{value}";
		labelBullet.label.dy = -20;

		chart2.cursor= new am4charts.RadarCursor();
	}
</script>
@endpush
