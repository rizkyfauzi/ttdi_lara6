<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\TTDIController;
use App\Http\Controllers\IPKNController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\BeritaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BerandaController::class, 'index'])->name('beranda');
Route::get('/ttdi', [TTDIController::class, 'index'])->name('ttdi');
Route::post('/ttdi/information', [TTDIController::class, 'information'])->name('ttdi/information');
Route::get('/ttdi/capaian', [TTDIController::class, 'capaian'])->name('ttdi/capaian');
Route::get('/ttdi/index', [TTDIController::class, 'index_table'])->name('ttdi/index');
Route::get('/ttdi/table', [TTDIController::class, 'capaian_table'])->name('ttdi/table');
Route::get('/ipkn', [IPKNController::class, 'index'])->name('ipkn');
Route::post('/ipkn/information', [IPKNController::class, 'information'])->name('ipkn/information');
Route::get('/ipkn/capaian', [IPKNController::class, 'capaian'])->name('ipkn/capaian');
Route::get('/ipkn/index', [IPKNController::class, 'index_table'])->name('ipkn/index');
Route::get('/data', [DataController::class, 'index'])->name('data');
Route::get('/data/search', [DataController::class, 'search'])->name('data/search');
Route::get('/berita', [BeritaController::class, 'index'])->name('berita');
Route::get('/berita/search', [BeritaController::class, 'search'])->name('berita/search');
Route::get('/berita/detail/{id}', [BeritaController::class, 'detail'])->name('berita/detail');
